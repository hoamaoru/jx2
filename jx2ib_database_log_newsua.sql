-- phpMyAdmin SQL Dump
-- version 5.0.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 26, 2021 at 03:39 PM
-- Server version: 5.5.68-MariaDB
-- PHP Version: 7.3.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jx2ib_database_log`
--

-- --------------------------------------------------------

--
-- Table structure for table `common_log`
--

CREATE TABLE `common_log` (
  `ID` int(11) NOT NULL,
  `LogTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `GroupName` varchar(32) DEFAULT NULL,
  `LogType` int(10) UNSIGNED DEFAULT NULL,
  `LogContent` blob,
  `ObjName` varchar(255) DEFAULT NULL,
  `Flag` tinyint(3) UNSIGNED DEFAULT NULL,
  `Editable` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `common_log`
--

INSERT INTO `common_log` (`ID`, `LogTime`, `GroupName`, `LogType`, `LogContent`, `ObjName`, `Flag`, `Editable`) VALUES
(1, '2021-01-16 14:42:16', '1', 4097, 0x4c61756e636853756363657373, 'Relay', 2, 0),
(2, '2021-01-16 16:54:55', '1', 4097, 0x4c61756e636853756363657373, 'Relay', 2, 0),
(3, '2021-01-17 04:46:50', '1', 36912, 0x00709e008ccb237aacbf0360000000000000000000000028, 'KTD001', 2, 0),
(4, '2021-01-17 04:46:55', '1', 36912, 0x00709e0031dfdbb2febf0360000000000000000000000028, 'KTD001', 2, 0),
(5, '2021-01-18 10:07:07', '1', 4097, 0x4c61756e636853756363657373, 'Relay', 2, 0),
(6, '2021-01-19 13:28:49', '1', 36912, 0x00709e00b195d0fe52de0660000000000000000000000028, 'MrChuTeo', 2, 0),
(7, '2021-01-19 15:21:32', '1', 36912, 0x00709e0094b6aa5da9de0660000000000000000000000028, 'MrChuTeo', 2, 0),
(8, '2021-01-19 15:33:48', '1', 36912, 0x00691200bf526bc249fb0660e541e541fc40003800380038, 'MrChuTeo', 2, 0),
(9, '2021-01-23 17:04:16', '1', 4097, 0x4c61756e636853756363657373, 'Relay', 2, 0),
(10, '2021-01-24 09:18:32', '1', 36864, 0x00675e503f1928e5bc330d607238733874385a0d290a900e, '1sTian', 3, 0),
(11, '2021-01-24 09:18:35', '1', 36864, 0x00655e506c6dcb06bc330d60723873387538cf0daa0bac0f, '1sTian', 3, 0),
(12, '2021-01-24 09:18:38', '1', 36864, 0x00645e50dd479e8bbc330d607238733876381f0e360f2d0d, '1sTian', 3, 0),
(13, '2021-01-24 09:18:43', '1', 36864, 0x00055e5049faf38abc330d608d3893389538520e88080d11, '1sTian', 3, 0),
(14, '2021-01-25 12:12:09', '1', 4097, 0x4c61756e636853756363657373, 'Relay', 2, 0),
(15, '2021-01-25 14:44:02', '1', 4097, 0x4c61756e636853756363657373, 'Relay', 2, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `common_log`
--
ALTER TABLE `common_log`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `LogTime` (`LogTime`),
  ADD KEY `GroupName` (`GroupName`),
  ADD KEY `LogType` (`LogType`),
  ADD KEY `ObjName` (`ObjName`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `common_log`
--
ALTER TABLE `common_log`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

