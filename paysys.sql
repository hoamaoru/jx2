-- phpMyAdmin SQL Dump
-- version 5.0.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 26, 2021 at 03:40 PM
-- Server version: 5.5.68-MariaDB
-- PHP Version: 7.3.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `paysys`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `id` int(11) NOT NULL,
  `username` varchar(32) NOT NULL DEFAULT 'testgame',
  `secpassword` varchar(64) NOT NULL DEFAULT 'e10adc3949ba59abbe56e057f20f883e',
  `password` varchar(64) NOT NULL DEFAULT 'a',
  `rowpass` varchar(32) DEFAULT 'a',
  `trytocard` int(1) NOT NULL DEFAULT '0',
  `changepwdret` int(1) NOT NULL DEFAULT '0',
  `active` int(1) NOT NULL DEFAULT '1',
  `LockPassword` int(11) NOT NULL DEFAULT '0',
  `trytohack` int(1) NOT NULL DEFAULT '0',
  `newlocked` int(1) NOT NULL DEFAULT '0',
  `locked` int(1) NOT NULL DEFAULT '0',
  `LastLoginIP` int(11) NOT NULL DEFAULT '0',
  `PasspodMode` int(11) NOT NULL DEFAULT '0',
  `email` varchar(64) NOT NULL DEFAULT 'diepbaotrung94@gmail.com',
  `cmnd` int(9) NOT NULL DEFAULT '123456780',
  `dob` date DEFAULT NULL,
  `coin` int(20) NOT NULL DEFAULT '0',
  `dateCreate` int(20) DEFAULT NULL,
  `lockedTime` datetime DEFAULT NULL,
  `testcoin` int(11) NOT NULL DEFAULT '9999999',
  `lockedCoin` int(10) NOT NULL DEFAULT '0',
  `bklactivenew` int(5) NOT NULL DEFAULT '0',
  `bklactive` int(5) NOT NULL DEFAULT '0',
  `nExtpoin1` int(5) NOT NULL DEFAULT '0',
  `nExtpoin2` int(5) NOT NULL DEFAULT '0',
  `nExtpoin4` int(5) NOT NULL DEFAULT '0',
  `nExtpoin5` int(5) NOT NULL DEFAULT '0',
  `nExtpoin6` int(5) NOT NULL DEFAULT '0',
  `nExtpoin7` int(5) NOT NULL DEFAULT '0',
  `scredit` int(10) NOT NULL DEFAULT '0',
  `nTimeActiveBKL` int(10) NOT NULL DEFAULT '0',
  `nLockTimeCard` int(15) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`id`, `username`, `secpassword`, `password`, `rowpass`, `trytocard`, `changepwdret`, `active`, `LockPassword`, `trytohack`, `newlocked`, `locked`, `LastLoginIP`, `PasspodMode`, `email`, `cmnd`, `dob`, `coin`, `dateCreate`, `lockedTime`, `testcoin`, `lockedCoin`, `bklactivenew`, `bklactive`, `nExtpoin1`, `nExtpoin2`, `nExtpoin4`, `nExtpoin5`, `nExtpoin6`, `nExtpoin7`, `scredit`, `nTimeActiveBKL`, `nLockTimeCard`) VALUES
(1, 'test', '0e698a8ffc1a0af622c7b4db3cb750cc', '0e698a8ffc1a0af622c7b4db3cb750cc', 'test01', 0, 0, 1, 0, 0, 0, 0, 1761716416, 0, '', 123456780, NULL, 9999999, NULL, NULL, 9999999, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0),
(2, 'hoamaoru', 'e10adc3949ba59abbe56e057f20f883e', 'e10adc3949ba59abbe56e057f20f883e', 'test01', 0, 0, 1, 0, 0, 0, 0, -571216725, 0, '', 123456780, NULL, 9998400, NULL, NULL, 9999999, 0, 0, 0, 0, 0, 0, 0, 0, 4857, 0, 0, 0),
(3, 'duong', '202cb962ac59075b964b07152d234b70', '202cb962ac59075b964b07152d234b70', 'test01', 0, 0, 1, 0, 0, 0, 0, 1694657908, 0, '', 123456780, NULL, 9998999, NULL, NULL, 9999999, 0, 0, 0, 0, 0, 0, 0, 0, 6341, 0, 0, 0),
(4, 'nhut', '202cb962ac59075b964b07152d234b70', '202cb962ac59075b964b07152d234b70', 'test01', 0, 0, 1, 0, 0, 0, 0, 804222762, 0, '', 123456780, NULL, 9999999, NULL, NULL, 9999999, 0, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0),
(5, 'anhlac', '202cb962ac59075b964b07152d234b70', '202cb962ac59075b964b07152d234b70', 'test01', 0, 0, 1, 0, 0, 0, 0, -1439615743, 0, '', 123456780, NULL, 9999999, NULL, NULL, 9999999, 0, 0, 0, 0, 0, 0, 0, 0, 254, 0, 0, 0),
(6, 'zzpthzz', '202cb962ac59075b964b07152d234b70', '202cb962ac59075b964b07152d234b70', 'test01', 0, 0, 1, 0, 0, 0, 0, -191234901, 0, '', 123456780, NULL, 9957026, NULL, NULL, 9999999, 0, 0, 0, 0, 0, 0, 0, 0, 1787, 0, 0, 0),
(7, 'trungtest', '202cb962ac59075b964b07152d234b70', '202cb962ac59075b964b07152d234b70', 'test01', 0, 0, 1, 0, 0, 0, 0, -571216725, 0, '', 123456780, NULL, 9999999, NULL, NULL, 9999999, 0, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0),
(8, 'chudai', 'e10adc3949ba59abbe56e057f20f883e', 'e10adc3949ba59abbe56e057f20f883e', 'test01', 0, 0, 1, 0, 0, 0, 0, -571216725, 0, '', 123456780, NULL, 9997699, NULL, NULL, 9999999, 0, 0, 0, 0, 0, 0, 0, 0, 4659, 0, 0, 0),
(9, 'truong', 'e10adc3949ba59abbe56e057f20f883e', 'e10adc3949ba59abbe56e057f20f883e', 'test01', 0, 0, 1, 0, 0, 0, 0, -780241622, 0, '', 123456780, NULL, 9999699, NULL, NULL, 9999999, 0, 0, 0, 0, 0, 0, 0, 0, 2085, 0, 0, 0),
(10, 'anh', '202cb962ac59075b964b07152d234b70', '202cb962ac59075b964b07152d234b70', 'test01', 0, 0, 1, 0, 0, 0, 0, 86669098, 0, '', 123456780, NULL, 9975853, NULL, NULL, 9999999, 0, 0, 0, 0, 0, 0, 0, 0, 3570, 0, 0, 0),
(11, 'anh2', '202cb962ac59075b964b07152d234b70', '202cb962ac59075b964b07152d234b70', 'test01', 0, 0, 1, 0, 0, 0, 0, 86669098, 0, '', 123456780, NULL, 9975593, NULL, NULL, 9999999, 0, 0, 0, 0, 0, 0, 0, 0, 3174, 0, 0, 0),
(12, 'chudai2', 'e10adc3949ba59abbe56e057f20f883e', 'e10adc3949ba59abbe56e057f20f883e', 'test01', 0, 0, 1, 0, 0, 0, 0, -571216725, 0, '', 123456780, NULL, 9997699, NULL, NULL, 9999999, 0, 0, 0, 0, 0, 0, 0, 0, 4561, 0, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account`
--
ALTER TABLE `account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

